#!/usr/bin/env python3
URL = "https://www.compassionintherapy.com/summit-home/"
OUTDIR = "~/4maggie/"
DB_FILE = "./status.pdb"

import pickledb
class Store:
    def __init__(self):
        self.db = pickledb.load(DB_FILE, True)

    def maybe_add_new_url(self, url):
        # returns True if this is really new
        if self.db.exists(url):
            return False
        else:
            self.db.dcreate(url)
            self.db.dadd(url, ("fetched", False))
            return True

    def get_next_url_to_fetch(self):
        # return the next to fetch or False for None
        for url in self.db.getall():
            if not self.db.dget(url, "fetched"):
                return url
        return False

    def set_fetched(self, url):
        self.db.dadd(url, ("fetched", True))

import requests
from bs4 import BeautifulSoup
import re, os
response = requests.get(URL)
soup = BeautifulSoup(response.text, 'html.parser')
divs = soup.findAll('div', attrs={"class": "tan-summit-video"})

def fetch_all_index_urls():
    urls = []
    for div in divs:
        try:
            url = div.a.attrs["href"]
            urls.append(url)
        except:
            # we're a 'dead' so the url is in the text
            print("ERROR PARSING", div)
    return urls

def process_page(url):
    resp = requests.get(url)
    soup = BeautifulSoup(resp.text, 'html.parser')
    divs = soup.findAll('div', attrs={"class": "elementor-widget-video"})
    data_settings = divs[0].attrs["data-settings"]
    watch_id = re.search(r"watch\?v=(.{11})", data_settings)
    if watch_id == None:
        watch_id = re.search(r"\.be\\/(.{11})", data_settings)
    watch_id = watch_id.group(1)
    youtube_url = f"https://www.youtube.com/watch?v={watch_id}"
    print(f"downloading {youtube_url}")
    os.system(f"yt-dlp --paths home:{OUTDIR} {youtube_url}")

if __name__ == '__main__':
    store = Store()
    urls = fetch_all_index_urls()
    for url in urls:
        store.maybe_add_new_url(url)
    print(store.db.getall())
    while True:
        url = store.get_next_url_to_fetch()
        if not url: break
        print(f"PROCESS:> {url}")
        process_page(url)
        store.set_fetched(url)
